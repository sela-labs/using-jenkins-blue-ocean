# Creating a Freestyle Job
---

## Preparation

 - Ask the instructor for the server IP and credentials

### Import the lab repository

 - Browse to the Gitlab server, login and create a new project
 
![Image 1](images/image-01.png)


 - Select import repository, repo by url and paste the url below
 
```
https://gitlab.com/sela-labs/demo-app.git
```

![Image 2](images/image-02.png)

 - Set the project name
 
```
demo-app-blue
```

![Image 3](images/image-03.png)

 - Set the visibility level as "Public"
 
![Image 4](images/image-04.png)

---

## Instructions

 - Go to the blueocean folder

![Image 5](images/image-05.png)


 - Browse to blue ocean portal
 
![Image 6](images/image-06.png)


 - Create a new job by click "New Pipeline" 

![Image 7](images/image-07.png)


 - Select git as source

![Image 8](images/image-08.png)


 - Set the repository url (ssh)
 
![Image 9](images/image-09.png)


 - Create a new ssh key in gitlab with the retrieved key, and then click "Create Pipeline"

![Image 10](images/image-10.png)

 - Configure the pipeline with the steps below (see the solution [here](./solution.txt)):

```
Stage 01 -> Get sources (git clone)
Stage 02 -> npm install
Stage 03 -> npm test
Stage 04 -> npm run build
Stage 05 -> Archive artifacts
(the job should run in your build server)
```

- Click save to create the Jenkinsfile and run the job

![Image 11](images/image-11.png)


 - Set the commit message to the Jenkinsfile creation and click save

![Image 12](images/image-12.png)


 - Inspect the build logs

![Image 13](images/image-13.png)

![Image 14](images/image-14.png)

 - Inspect the sources repository

![Image 15](images/image-15.png)

---

## Optional

 - Create a new branch and update the Jenkinsfile

 - Open the job using blue ocean