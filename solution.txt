pipeline {
  agent {
    node {
      label '34.77.152.3'
    }

  }
  stages {
    stage('Restore') {
      steps {
        sh 'npm install'
      }
    }
    stage('Test') {
      steps {
        sh 'npm run test'
      }
    }
    stage('Build') {
      steps {
        sh 'npm run build'
      }
    }
    stage('Archive Artifacts') {
      steps {
        archiveArtifacts '*.zip'
      }
    }
  }
}